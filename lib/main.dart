//Name: Carlitos Carmona
//Email: carmo025@cougars.csusm.edu
//Professor: Zampell
//Class: CS 481 T-TH 7:00pm-8:15pm
//HW2 Layout Design - The purpose of this assignment is to design an app using
//features such as rows,columns and icons. Applying widgets can help encapsulate
//certain information and design. I created an app about my two dogs. Enjoy.


import 'package:flutter/material.dart';

//STARTING POINT ABSOLUTE MINIMUM TO RUN
void main()
{
  runApp(MyApp());
}
//FURTHER ASSIGNMENTS WILL BE WORKED ON HERE

//Will BE USING TWO TYPES OF WIDGETS IN FLUTTER
//IN THIS CASE A STATELESS WIDGET WILL CONSIST OF OUR CONTAINER
//HOLDING OUR WIDGETS
//IN ORDER TO CUSTOMIZE OUR ROWS AND COLUMNS

//CONTAINER -> ROW -> - COLUMN -> BRANCHES-> ICON or -> NEW CONTAINER -> TEXT
class MyApp extends StatelessWidget {
  //INITIALIZES CONTAINER
  Widget titleSection = Container(
    padding: const EdgeInsets.all(32),
    child: Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  //TEXT ROW #1
                  // FIRST ROW OF TEXT
                  'Meet Charlotte on the left and Ryder on the right',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Text(
                // TEXT ROW #2
                // SECOND ROW OF TEXT
                'Charlotte is a Gerberian Shepsky'
                    ' & Ryder is a Siberian Husky',
                style: TextStyle(
                  // SETS THE COLORS TEXT TO BLUEGREY
                  color: Colors.blueGrey[500],
                ),
              ),
            ],
          ),
        ),
        //SET ICON AND COLOR USED FOR LAYOUT
        Icon(
          Icons.favorite,
          color: Colors.redAccent[500],
        ),
        //TEXT FOR AMOUNT OF LOVE THE PETS GIVE AND GET
        Text('infinite'),
      ],
    ),
  );


// DEFINE A SPECIFIC OBJECT _BUILD BUTTON COLUMN
  // INCLUDES A TEXT ITEM AND ICON

  @override
  Widget build(BuildContext context) {
    // BACKGROUND COLOR TYPE IS DEFAULT TO FLUTTER
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        //SPACE EVENLY CAN AUTOMATICALLY ARRANGE ITEMS TO FIT SCOPE
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          // SEARCHED FOR ICONS IN MATERIALS LIBRARY
          //CREATE BUTTONS WITH DESCRIPTORS
          _buildButtonColumn(color, Icons.flag, 'RACE'),
          _buildButtonColumn(color, Icons.brightness_3, 'HOWL'),
          _buildButtonColumn(color, Icons.cake, 'SWEETS'),
        ],
      ),
    );

// FILL DESCRIPTOR TEXT WITH APPROPRIATE INFORMATION
    // SINGLE QUOTATIONS AND SPACING IS IMPORTANT TO LAYOUT DESIGN
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        // ESSAY TIME...
        'Let me start off by telling you that Charlotte and Ryder are a handful.'
            'They are always energetic and constantly know how to get you up and '
            'off your seat. When they both go to the dog beach they love to race '
            'with each other. Although, Ryder only weighs 50 lbs and Charlotte we'
            'ighs 90 lbs. You can guess who wins most races. My dogs definitely '
            'make my days a whole lot better, they are my excuse to go on new adventures.',
            // INCLUDE SOFT WRAP
          softWrap: true,
      ),
    );

// CONTROLS MY WIDGETS,BUTTONS,ICONS...ETC
    return MaterialApp(
      //TITLE MY ASSIGNMENT
      title: 'Layout Homework',
      // MATERIAL DESIGN LAYOUT STRUCTURE
      home: Scaffold(
        //CREATES A BAR AT THE TOP OF THE SCREEN
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        // ENCAPSULATE THE FOLLOWING CODE WITHIN LIST VIEW
        // ALLOWS SCROLLING FOR SMALLER DEVICES
        body: ListView(
          children: [
            Image.asset(
              // IMPLEMENTS IMAGE FROM DIRECTORY - IMAGES/PETS.JPG
              'images/pets.jpg',
              //FITS THE IMAGE INTO THE LAYOUT DESIGN
              width: 600,
              height: 240,
              fit: BoxFit.cover,

            ),
            //ENCAPSULATE MY BUTTONS
            titleSection,
            buttonSection,
            textSection,
    ],
      ),
    ),
    );
  }// END OF BUILD

  //STRUCTURE FOR MY BUTTON COLUMN
  // INITIALIZES THE COLOR, ICONS AND NAME FOR ICON
  //MAINAXIS_SIZE AND MAINAXIS_ALIGNMENT CONTROLS THE SIZE AND POSITION

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ], // END OF CHILDREN
    );
  }// END OF BUILD BUTTON
} //END OF MY APP